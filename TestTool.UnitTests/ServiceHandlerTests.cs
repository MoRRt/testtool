﻿using NUnit.Framework;
using TestTool.SearchManagers;

namespace TestTool.UnitTests
{
    [TestFixture]
    public class ServiceHandlerTests
    {
        [Test]
        public void IsValidPath_CorrectPath_ReturnTrue()
        {
            ServiceHandler serviceHandler = new ServiceHandler(new BaseManager());
            bool result = serviceHandler.IsValidPath(@"C:\Windows\");
            Assert.True(result);
        }

        [Test]
        public void IsValidPath_IncorrectPath_ReturnFalse()
        {
            ServiceHandler serviceHandler = new ServiceHandler(new BaseManager());
            bool result = serviceHandler.IsValidPath("sgsdgdghnsh");
            Assert.False(result);
        }
    }
}
