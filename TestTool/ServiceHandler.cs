﻿using System;
using System.Text.RegularExpressions;
using TestTool.SearchManagers;

namespace TestTool
{
    public class ServiceHandler
    {
        private ISearchManager _searchManager;

        public ServiceHandler(ISearchManager searchManager)
        {
            _searchManager = searchManager;
        }

        public  void Handle(string searchPath, string outputPath, string param)
        {
            try
            {
                string[] searchResult;
                switch (param)
                {
                    case "all":
                        searchResult =  _searchManager.SearchByParam(searchPath);
                        _searchManager.PrintToFile(searchResult, searchPath, outputPath);
                        break;
                    case "cpp":
                        searchResult = _searchManager.SearchByParam(searchPath, param);
                        _searchManager.PrintToFile(searchResult, searchPath, outputPath);
                        break;
                    case "reserved1":
                        _searchManager = new FirstVariantSearchManager();
                        searchResult =  _searchManager.SearchByParam(searchPath);
                        _searchManager.PrintToFile(searchResult, searchPath, outputPath);
                        break;
                    case "reserved2":
                        _searchManager = new SecondVariantSearchManager();
                        searchResult = _searchManager.SearchByParam(searchPath);
                        _searchManager.PrintToFile(searchResult, searchPath, outputPath);
                        break;
                    default:
                        throw new Exception("Invalid command");
                }
                
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Error!");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(e.Message);
            }
        }

        public bool IsValidPath(string path)
        {
            return !string.IsNullOrEmpty(path) &&
                   Regex.IsMatch(path, @"\A(?:\b[a-z]:\\(?:[^\\/:*?""<>|\r\n]+\\)*[^\\/:*?""<>|\r\n]*)\Z",
                       RegexOptions.IgnoreCase);
        }
    }
}