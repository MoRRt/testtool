﻿using System;
using System.IO;

namespace TestTool.SearchManagers
{
    public class SecondVariantSearchManager : BaseManager
    {
        public override void PrintToFile(string[] resultSearch, string searchPath, string outputFile)
        {
            FileInfo fi = new FileInfo(outputFile);
            using (StreamWriter writter = fi.CreateText())
            {
                foreach (var path in resultSearch)
                {
                    char[] tempArray = path.Replace(searchPath, "").ToCharArray();
                    Array.Reverse(tempArray);
                    writter.WriteLine(new string(tempArray));
                }
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }
    }
}