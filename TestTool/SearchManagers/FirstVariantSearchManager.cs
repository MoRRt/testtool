﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TestTool.SearchManagers
{
    public class FirstVariantSearchManager : BaseManager
    {
        public override void PrintToFile(string[] resultSearch, string searchPath, string outputFile)
        {
            FileInfo fi = new FileInfo(outputFile);
            using (StreamWriter writter = fi.CreateText())
            {
                foreach (var path in resultSearch)
                {
                    string updatePath = path.Replace(searchPath, "");
                    IEnumerable<string> piecesEnumerable = updatePath.Split(new[]{ '\\' }, StringSplitOptions.RemoveEmptyEntries).Reverse().ToList();
                    StringBuilder finalPath = new StringBuilder();
                    foreach (var piece in piecesEnumerable)
                    {
                        finalPath.Append(piece + @"\");
                    }
                    writter.WriteLine(finalPath);
                }
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }
    }
}