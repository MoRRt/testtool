﻿using System.Threading.Tasks;

namespace TestTool
{
    public interface ISearchManager
    {
        string[] SearchByParam(string param, string searchPath = null);
        void PrintToFile(string[] resultSearch, string searchPath, string outputFile);
    }
}