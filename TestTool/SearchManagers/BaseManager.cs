﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace TestTool.SearchManagers
{
    public class BaseManager : ISearchManager
    {
        public virtual void PrintToFile(string[] resultSearch, string searchPath,string outputFile)
        {
            FileInfo fi = new FileInfo(outputFile);
            using (StreamWriter writter = fi.CreateText())
            {
                foreach (var path in resultSearch)
                {
                    writter.WriteLine(path.Replace(searchPath, ""));
                }
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Success");
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        public string[] SearchByParam(string searchPath, string param = null)
        {
            return Directory.GetFiles(searchPath, "*" + param, SearchOption.AllDirectories);
        }
    }
}