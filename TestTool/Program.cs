﻿using System;
using System.IO;
using System.Reflection;
using Ninject;

namespace TestTool
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length <= 0)
                   throw new Exception("Please, enter the command");
                var kernel = new StandardKernel();
                kernel.Load(Assembly.GetExecutingAssembly());
                ISearchManager searchManager = kernel.Get<ISearchManager>();
                ServiceHandler serviceHandler = new ServiceHandler(searchManager);
                if (!serviceHandler.IsValidPath(args[0]) && !Directory.Exists(args[0]))
                    throw new Exception("Invalid search path");
                string searchPath = args[0];
                string param = args[1];
                if (args.Length == 3 && !serviceHandler.IsValidPath(args[2]))
                    throw new Exception("Invalid output path");
                string outputPath = args.Length == 2? @"C:\result.txt" : args[2];
                serviceHandler.Handle(searchPath, outputPath, param);
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("Error!");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(e.Message);
            }
        }
    }
}
