﻿using Ninject.Modules;
using TestTool.SearchManagers;

namespace TestTool
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<ISearchManager>().To<BaseManager>();
        }
    }
}